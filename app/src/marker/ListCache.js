import {
    FlatList,
    Text,
    View
} from "react-native";
import React, { useEffect, useState } from "react";

import CacheRow from './CacheRow'

const rootStyle = {
    display: "flex",
    flexDirection: "column",
    padding: 64,
    alignItems: "center",
};

const ListCache = ({ markers }) => {
    const [isEmpty, setIsEmpty] = useState(false);

    useEffect(() => {
        if (markers == undefined) {
            setIsEmpty(true)
        }
    }, [])


    const _renderItem = ({ item }) => (
        <CacheRow title={item.title} hint={item.hint} difficulty={item.difficulty} />
    )

    const _renderSeparator = () => <View style={{ height: 1, backgroundColor: 'grey' }} />

    return isEmpty ?
        <View style={rootStyle}>
            <Text>Sorry there is no cache available at proximity</Text>
        </View>
        :
        <View style={rootStyle}>
            <FlatList
                data={markers}
                renderItem={_renderItem}
                keyExtractor={cache => cache.title}
                ItemSeparatorComponent={_renderSeparator}
            />
        </View>;
};

export default ListCache;