import { Image, StyleSheet, Text, View } from 'react-native'

import React from 'react'

export default CacheRow = props => (
    <View style={styles.row}>
        <View>
            <Text style={styles.primaryText}>{props.title}</Text>
            <Text style={styles.secondaryText}>{props.hint}</Text>
            <Text style={styles.secondaryText}>Difficulty : {props.difficulty}</Text>
        </View>
    </View>
)

const styles = StyleSheet.create({
    row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
    primaryText: {
        fontWeight: 'bold',
        fontSize: 14,
        color: 'black',
        marginBottom: 4,
    },
    secondaryText: { color: 'grey' },
})