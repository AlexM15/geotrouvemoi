import {
    ActivityIndicator,
    Alert,
    Button,
    Text,
    TextInput,
    View,
} from "react-native";
import React, { useState } from "react";

import fetchHelper from "../data/fetchHelper";

const rootStyle = {
    display: "flex",
    flexDirection: "column",
    padding: 64,
    alignItems: "center",
};

const inputStyle = {
    borderWidth: 1,
    borderRadius: 4,
    padding: 8,
    backgroundColor: "#fff",
    width: "100%",
    marginBottom: 32,
};

const CreateMarker = ({ latitude, longitude, user, navigation }) => {
    const [title, setTitle] = useState("");
    const [hint, setHint] = useState("");
    const [difficulty, setDifficulty] = useState("");
    const [loading, setLoading] = useState(false);

    const create = async () => {
        setLoading(true);
        try {
            const cache = await fetchHelper.createMarker(title, latitude, longitude, hint, difficulty, user);
            setLoading(false);
            navigation.navigate('Home')
        } catch (err) {
            Alert.alert("Erreur", err.message);
            setLoading(false);
        }
    };

    const onCheckLimit = (value) => {
        const parsedDty = Number.parseInt(value)
        if (Number.isNaN(parsedDty) && value != "") {
            setDifficulty("0")
        } else if (parsedDty > 10) {
            setDifficulty("10")
        }
        else if (parsedDty < 0) {
            setDifficulty("0")
        } else {
            if (Number.isNaN(Math.round(value))) {
                value = "0"
            }
            else {
                value = Math.round(value).toString()
            }
            setDifficulty(value)
        }
    }

    return (
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Create Marker
            </Text>
            <Text>Title</Text>
            <TextInput
                style={inputStyle}
                value={title}
                onChangeText={(txt) => setTitle(txt)}
            />
            <Text>Difficulty</Text>
            <TextInput
                style={inputStyle}
                value={difficulty}
                keyboardType="numeric"
                onChangeText={onCheckLimit}
            />
            <Text>Hint</Text>
            <TextInput
                style={inputStyle}
                value={hint}
                onChangeText={(txt) => setHint(txt)}
            />
            {loading ? (
                <ActivityIndicator size={32} color="blue" />
            ) : (
                <Button title="Create" onPress={create} />
            )}
        </View>
    )
};

export default CreateMarker;