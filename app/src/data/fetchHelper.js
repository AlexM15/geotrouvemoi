const fetchHelper = {
    getUser: async (userId) =>
        fetch(
            `https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/user?userId=${userId}`,
            {
                headers: { "Content-Type": "application/json" },
                method: "GET",
            }
        ).then((response) => response.json()), //besoin gestion type reponse
    updateAvatarForUser: async (userId, avatar) =>
        fetch(
            `https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/user?userId=${userId}`,
            {
                headers: { "Content-Type": "application/json" },
                method: "PATCH",
                body: JSON.stringify({ avatar }),
            }
        ).then((response) => response.json()),
    editUser: async (userId, name) =>
        fetch(`https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/user?userId=${userId}`,
            {
                headers: { "Content-Type": "application/json" },
                method: "PATCH",
                body: JSON.stringify({
                    name
                }),
            }
        ).then((response) => response.json()),
    createMarker: async (title, latitude, longitude, hint, difficulty, user) =>
        fetch(`https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/cache`,
            {
                headers: { "Content-Type": "application/json" },
                method: "POST",
                body: JSON.stringify({
                    title,
                    latitude,
                    longitude,
                    hint,
                    difficulty,
                    user
                }),
            }
        ).then((response) => response.json()),
    getMarkers: async () =>
        fetch(`https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/cache`,
            {
                headers: { "Content-Type": "application/json" },
                method: "GET",
            }
        ).then((response) => response.json()),
    getMarker: async (cacheId) =>
        fetch(
            `https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/user?cacheId=${cacheId}`,
            {
                headers: { "Content-Type": "application/json" },
                method: "GET",
            }
        ).then((response) => response.json()), //besoin gestion type reponse
    createUser: async (email, name) =>
        fetch(`https://europe-west1-geotrouvemoi-330406.cloudfunctions.net/user`,
            {
                headers: { "Content-Type": "application/json" },
                method: "POST",
                body: JSON.stringify({
                    email,
                    name
                }),
            }
        ).then((response) => response.json()),
};

export default fetchHelper;