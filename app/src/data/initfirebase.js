import { getApps, initializeApp } from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyAidCsOErTcYPRPe2MMA8dzPxjpGdFs1Yc",
    authDomain: "geotrouvemoi-330406.firebaseapp.com",
    projectId: "geotrouvemoi-330406",
    storageBucket: "geotrouvemoi-330406.appspot.com",
    messagingSenderId: "291124454601",
    appId: "1:291124454601:web:4dc5fdfa8276e8b959d918",
    measurementId: "G-DZP8FLNGWQ"
};

if (getApps().length === 0) {
    initializeApp(firebaseConfig);
}
