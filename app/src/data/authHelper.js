import { getAuth, sendPasswordResetEmail, signInWithEmailAndPassword, createUserWithEmailAndPassword } from "firebase/auth";

const auth = getAuth();

const authHelper = {
    signInOnFirebase: async (email, password) => {
        const result = await signInWithEmailAndPassword(
            auth,
            email,
            password
        );
        return result.user.email;
    },
    resetPassword: async (email) => {
        const result = await sendPasswordResetEmail(
            auth,
            email,
        );
        return result
    },
    createUser: async (email, password) => {
        const result = await createUserWithEmailAndPassword(
            auth,
            email,
            password
        );
        return result.user.email;
    },
};

export default authHelper;