import * as ImagePicker from "expo-image-picker";

import {
    Button,
    Image,
    RefreshControl,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import React, { useEffect, useState } from "react";
import {
    getDownloadURL,
    getStorage,
    ref,
    uploadBytes,
} from "firebase/storage";

import fetchHelper from "../data/fetchHelper";
import { v4 as uuidv4 } from "uuid";

async function uploadImageAsync(uri, type) {
    // Why are we using XMLHttpRequest? See:
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662
    const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = function () {
            resolve(xhr.response);
        };
        xhr.onerror = function (e) {
            console.log(e);
            reject(new TypeError("Network request failed"));
        };
        xhr.responseType = "blob";
        xhr.open("GET", uri, true);
        xhr.send(null);
    });

    const storage = getStorage();
    const fileRef = ref(
        storage,
        `${uuidv4()}.${type === "image/png" ? "png" : "jpg"}`
    );
    const result = await uploadBytes(fileRef, blob);

    // We're done with the blob, close and release it
    blob.close();

    const url = await getDownloadURL(result.ref);

    return url;
}

const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
};

export default User = ({ user, userData, signOut, changeAvatarUrl, navigation }) => {
    const [userdata, setUserData] = useState(userData);

    const getUserServer = async () => {
        const serverUserData = await fetchHelper.getUser(user);
        setUserData(serverUserData);
    }

    useEffect(() => {
        (async () => {
            const { status } =
                await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== "granted") {
                alert(
                    "L'app n'accède à vos photos que pour définir votre avatar."
                );
            }
        })();
        const unsubscribe = navigation.addListener('focus', () => {
            getUserServer()
        });

        return unsubscribe;
    }, [navigation]);

    const uploadImage = async (imgUri, type) => {
        try {
            const url = await uploadImageAsync(imgUri, type);
            changeAvatarUrl(url);
        } catch (err) {
            console.log(err);
        }
    };

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            uploadImage(result.uri, result.type);
        }
    };

    return (
        <View style={{ padding: 16 }}>
            <View
                style={{
                    padding: 32,
                    borderRadius: 8,
                    borderWidth: 1,
                    backgroundColor: "#fff",
                    display: "flex",
                    alignItems: "center",
                }}
            >
                <TouchableOpacity onPress={pickImage}>
                    <View
                        style={{
                            backgroundColor: "#888",
                            borderRadius: 100,
                            width: 200,
                            height: 200,
                            alignItems: "center",
                            justifyContent: "center",
                            marginBottom: 32,
                        }}
                    >
                        {userdata.avatar ? (
                            <Image
                                source={{ uri: userdata.avatar }}
                                style={{
                                    borderRadius: 100,
                                    width: 200,
                                    height: 200,
                                }}
                            />
                        ) : (
                            <Text
                                style={{
                                    fontWeight: "bold",
                                    fontSize: 48,
                                    color: "#fff",
                                }}
                            >
                                ?
                            </Text>
                        )}
                    </View>
                </TouchableOpacity>
                <Text
                    style={{
                        fontSize: 24,
                        fontWeight: "bold",
                        marginBottom: 32,
                    }}
                >
                    {userdata.name}
                </Text>
                <Button title="Edit informations" onPress={() => navigation.navigate('Edit User')} />
                <Button title="Log out" onPress={signOut} />
            </View>
        </View>
    );
};