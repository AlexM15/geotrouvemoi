import {
    ActivityIndicator,
    Alert,
    Button,
    Text,
    TextInput,
    View,
} from "react-native";
import React, { useEffect, useState } from "react";

// import authHelper from "../data/authHelper";
import fetchHelper from "../data/fetchHelper";
import authHelper from "../data/authHelper";

const rootStyle = {
    display: "flex",
    flexDirection: "column",
    padding: 64,
    alignItems: "center",
};

const inputStyle = {
    borderWidth: 1,
    borderRadius: 4,
    padding: 8,
    backgroundColor: "#fff",
    width: "100%",
    marginBottom: 32,
};

const Register = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [isCreated, setIsCreated] = useState(false);

    const register = async () => {
        setLoading(true);
        try {
            //Création de l'utilisateur en appelant les fonctions Firebase
            const authUser = await authHelper.createUser(email, password);
            const user = await fetchHelper.createUser(email, name);
            setLoading(false);
            setIsCreated(true);
        } catch (err) {
            Alert.alert("Erreur", err.message);
            setLoading(false);
        }
    };

    return isCreated ?
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Register
            </Text>
            <Text style="color:green">Your account has been created, go back to the login page now ;)</Text>
        </View>
        :
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Register
            </Text>
            <Text>Username</Text>
            <TextInput
                style={inputStyle}
                value={name}
                onChangeText={(txt) => setName(txt)}
            />
            <Text>Email</Text>
            <TextInput
                style={inputStyle}
                value={email}
                onChangeText={(txt) => setEmail(txt)}
            />
            <Text>Password</Text>
            <TextInput
                style={inputStyle}
                secureTextEntry={true}
                value={password}
                onChangeText={(txt) => setPassword(txt)}
            />
            {loading ? (
                <ActivityIndicator size={32} color="blue" />
            ) : (
                <Button title="Register" onPress={register} />
            )}
        </View>;
};

export default Register;