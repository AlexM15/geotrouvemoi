import {
    ActivityIndicator,
    Alert,
    Button,
    Text,
    TextInput,
    View,
} from "react-native";
import React, { useState } from "react";

import authHelper from "../data/authHelper";

const rootStyle = {
    display: "flex",
    flexDirection: "column",
    padding: 64,
    alignItems: "center",
};

const inputStyle = {
    borderWidth: 1,
    borderRadius: 4,
    padding: 8,
    backgroundColor: "#fff",
    width: "100%",
    marginBottom: 32,
};

const Auth = ({ setUser, navigation }) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);

    const signIn = async () => {
        setLoading(true);
        try {
            const user = await authHelper.signInOnFirebase(email, password);
            await setUser(user);
            setLoading(false);
        } catch (err) {
            Alert.alert("Error", err.message);
            setLoading(false);
        }
    };
    
    return (
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Log in
            </Text>
            <Text>Email</Text>
            <TextInput
                style={inputStyle}
                value={email}
                onChangeText={(txt) => setEmail(txt)}
            />
            <Text>Password</Text>
            <TextInput
                style={inputStyle}
                secureTextEntry
                value={password}
                onChangeText={(txt) => setPassword(txt)}
            />
            {loading ? (
                <ActivityIndicator size={32} color="blue" />
            ) : (
                <Button title="Log in" onPress={signIn} />
            )}

            <Button title="Forgot Password ?" onPress={() => navigation.navigate('Forgot Password')} />

            <Button title="Register" onPress={() => navigation.navigate('Register')} />

        </View>
    );
};

export default Auth;