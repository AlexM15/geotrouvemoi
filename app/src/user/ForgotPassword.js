import {
    ActivityIndicator,
    Alert,
    Button,
    Text,
    TextInput,
    View,
} from "react-native";
import React, { useEffect, useState } from "react";

// import authHelper from "../data/authHelper";
import fetchHelper from "../data/fetchHelper";
import authHelper from "../data/authHelper";

const rootStyle = {
    display: "flex",
    flexDirection: "column",
    padding: 64,
    alignItems: "center",
};

const inputStyle = {
    borderWidth: 1,
    borderRadius: 4,
    padding: 8,
    backgroundColor: "#fff",
    width: "100%",
    marginBottom: 32,
};

const ForgotPassword = () => {
    const [email, setEmail] = useState("");
    const [loading, setLoading] = useState(false);
    const [isCreated, setIsCreated] = useState(false);

    const forgotPassword = async () => {
        setLoading(true);
        try {
            //Envoi du mail pour le reset password en appelant les fonctions Firebase
            const resetPwd = await authHelper.resetPassword(email);
            setLoading(false);
            setIsCreated(true);
        } catch (err) {
            Alert.alert("Erreur", err.message);
            setLoading(false);
        }
    };

    return isCreated ?
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Reset your password
            </Text>
            <Text style="color:green">A mail has been sent to your email adress ;)</Text>
        </View>
        :
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Reset your password
            </Text>
            <Text>Enter your email adress to reset your password</Text>
            <TextInput
                style={inputStyle}
                value={email}
                onChangeText={(txt) => setEmail(txt)}
            />
            {loading ? (
                <ActivityIndicator size={32} color="blue" />
            ) : (
                <Button title="Reset" onPress={forgotPassword} />
            )}
        </View>;
};

export default ForgotPassword;