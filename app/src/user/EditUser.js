import {
    ActivityIndicator,
    Alert,
    Button,
    Text,
    TextInput,
    View,
} from "react-native";
import React, { useEffect, useState } from "react";

import authHelper from "../data/authHelper";
import fetchHelper from "../data/fetchHelper";

const rootStyle = {
    display: "flex",
    flexDirection: "column",
    padding: 64,
    alignItems: "center",
};

const inputStyle = {
    borderWidth: 1,
    borderRadius: 4,
    padding: 8,
    backgroundColor: "#fff",
    width: "100%",
    marginBottom: 32,
};

const EditUser = ({ user, navigation }) => {
    const [name, setName] = useState("");
    const [loading, setLoading] = useState(false);
    const [resetPressed, setResetPressed] = useState(true);

    const edit = async () => {
        setLoading(true);
        try {
            //Modification de l'utilisateur en appelant les fonctions Firebase
            const editUser = await fetchHelper.editUser(user, name);
            setLoading(false);
            navigation.navigate('User');
        } catch (err) {
            Alert.alert("Erreur", err.message);
            setLoading(false);
        }
    };

    const resetPasswordButton = (user) => {
        authHelper.resetPassword(user);
        setResetPressed(false);
    }

    return (
        <View style={rootStyle}>
            <Text style={{ fontSize: 24, marginBottom: 32 }}>
                Edit User
            </Text>
            <Text>Username</Text>
            <TextInput
                style={inputStyle}
                value={name}
                onChangeText={(txt) => setName(txt)}
            />
            {resetPressed ? (
                <Button title="Reset Password" onPress={() => resetPasswordButton(user)} />
            ) : (
                <Text>A mail has been sent to your email adress.</Text>
            )}
            {loading ? (
                <ActivityIndicator size={32} color="blue" />
            ) : (
                <Button title="Edit" onPress={edit} />
            )}
        </View>
    )
};

export default EditUser;