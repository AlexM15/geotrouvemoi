import 'react-native-gesture-handler';
import './src/data/initfirebase.js'

import * as BackgroundFetch from 'expo-background-fetch';
import * as Localization from 'expo-localization';
import * as Location from 'expo-location';
import * as Notifications from 'expo-notifications';
import * as TaskManager from 'expo-task-manager';

import { ActivityIndicator, Button, Dimensions, Image, StyleSheet, Text, View } from "react-native";
import MapView, { Marker } from 'react-native-maps';
import React, { useEffect, useRef, useState } from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import Auth from "./src/user/Auth";
import Constants from 'expo-constants';
import CreateMarker from "./src/marker/CreateMarker";
import EditUser from "./src/user/EditUser";
import ForgotPassword from "./src/user/ForgotPassword";
import ListCache from "./src/marker/ListCache"
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { NavigationContainer } from "@react-navigation/native";
import Register from "./src/user/Register";
import { StatusBar } from "expo-status-bar";
import User from "./src/user/User";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import fetchHelper from "./src/data/fetchHelper";
import i18n from 'i18n-js';

//Création des variables pour les menus
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

//Création de la variable pour les notifs
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

/////////////////////////////GEO-LOCALISATION LANGUE//////////////////////////////////

// Set the key-value pairs for the different languages you want to support.
i18n.translations = {
  en: {
    welcome: 'Hello',
    text: 'Have fun with GeoTrouveMoi'
  },
  fr: {
    welcome: 'Bonjour',
    text: 'Amusez vous bien avec GeoTrouveMoi'
  },
  ja: {
    welcome: 'こんにちは',
    text: 'GeoTrouveMoi を楽しんでください'
  },
};
// Set the locale once at the beginning of your app.
i18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;

/////////////////////////////FIN GEO-LOCALISATION LANGUE//////////////////////////////////

const App = ({ navigation }) => {
  /////////////////////////////GEO-LOCALISATION COORDONNEES//////////////////////////////////
  const [locationResult, setLocation] = React.useState(null)
  const [mapRegion, setRegion] = React.useState(null)
  const [isLocationActivate, setLocationStatus] = React.useState(false)
  const [message, setMessage] = React.useState("")

  const getLocationAsync = async () => {

    let isLocationActivate = await Location.hasServicesEnabledAsync()

    if (isLocationActivate) {

      setLocationStatus(true)

      let { status } = await Location.requestForegroundPermissionsAsync()
      if ('granted' !== status) {
        setMessage("Location permissions are not granted.")
      } else {
        setMessage("Finding your current location...")

        await Location.watchPositionAsync(
          {
            accuracy: Location.Accuracy.High,
            timeInterval: 5000,
            distanceInterval: 80,
          },
          (location_update) => {
            let latitude = location_update.coords.latitude
            let longitude = location_update.coords.longitude
            setLocation({ latitude, longitude });

            // Center the map on the location we just fetched.
            setRegion({
              latitude,
              longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            });
          }
        )
      }
    }
    else {
      setMessage("Please enable location service on your device.")
    }
  }
  //////////////////////FIN GEO-LOCALISATION COORDONNEES//////////////////////////////////////

  ////////////////// Tache de fond ///////////////////////

  const BACKGROUND_FETCH_TASK = 'background-fetch';

  TaskManager.defineTask(BACKGROUND_FETCH_TASK, async () => {
    try {
      // Tableaux des météos considérées comme belles
      let goodWeather = [1, 2, 3, 4, 8, 9, 10, 11, 14, 27, 28, 29, 30];
      fetch(
        'https:///weather.api.here.com/weather/1.0/report.json?product=observation&latitude=' + locationResult.latitude + '&longitude=' + locationResult.longitude + '&oneobservation=true&apiKey=D7bO43p_Sfd2EaxFxtZJhXoEbB6464AuvthEVxCG7HA',
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
          }
        }
      )
        .then(result => {
          return result.json();
        }).then(json => {
          //Récupération des données de l'API
          const weather = parseInt(json.observations.location[0].observation[0].skyInfo);
          const daylight = json.observations.location[0].observation[0].daylight;
          console.log('weather: ' + weather + ", tableau: " + goodWeather.includes(weather) + ", daylight: " + daylight);

          //Si il fait beau et qu'il fait jour
          if (goodWeather.includes(weather) && daylight == 'D') {
            //On envoie une notif à l'utilisateur
            Notifications.scheduleNotificationAsync({
              content: {
                title: '☀️ Il fait beau aujourd\'hui !',
                body: 'C\'est le temps parfait pour aller trouver des caches sur GéoTrouveMoi !'
              },
              trigger: { seconds: 2 },
            });
          }else{
            Notifications.scheduleNotificationAsync({
              content: {
                title: '🌧 Il ne fait pas beau aujourd\'hui !',
                body: 'C\'est pas le temps parfait pour aller trouver des caches sur GéoTrouveMoi !'
              },
              trigger: { seconds: 2 },
            });
          }
        });
    } catch (err) {
      console.log("Erreur Background Task : " + err);
    }

    return BackgroundFetch.Result.NewData;
  });

  async function registerBackgroundFetchAsync() {
    //Intervalle de temps pour que le Background Fetch s'execute
    return BackgroundFetch.registerTaskAsync(BACKGROUND_FETCH_TASK, {
      minimumInterval: 30, // en secondes 60 * 60 * 24 * 3
      stopOnTerminate: false,
      startOnBoot: true,
    });
  }

  //////////////////// Fin Tache de fond /////////////////////

  //////////////////////CONNEXION///////////////////////////////
  //Déclaration des variables
  const [userLoading, setUserLoading] = React.useState(true);
  const [user, setUser] = React.useState(null);
  const [userData, setUserData] = React.useState(null);

  //Utile ? à vérifier
  const USER_STORAGE_KEY = "USER_STORAGE_KEY";
  const AVATAR_STORAGE_KEY = "AVATAR_STORAGE_KEY";

  const initialProcess = async () => {
    try {
      const storageUser = await AsyncStorage.getItem(
        USER_STORAGE_KEY
      );
      if (storageUser) {
        // User is authenticated
        setUser(storageUser);
        const serverUserData = await fetchHelper.getUser(
          storageUser
        );
        setUserData(serverUserData);
        // fetchAndSetData();
      } else {
        // User is anonymous
      }
      setUserLoading(false);
    } catch (err) {
      console.error(err);
    }
  };

  //Déconnexion
  const signOut = () => {
    AsyncStorage.removeItem(USER_STORAGE_KEY);
    AsyncStorage.removeItem(AVATAR_STORAGE_KEY);
    setUser(null);
    setUserData(null);
  };
  ////////////////FIN DE CONNEXION////////////////////////////

  //////////////////DEBUT GESTION USER///////////////////////
  //Changement d'avatar
  const changeAvatarUrl = (newAvatar) => {
    setUserData({ ...userData, avatar: newAvatar });
    fetchHelper.updateAvatarForUser(user, newAvatar);
  };

  //Récup infos user (avatar et name actuellement)
  const setUserAndFetchData = async (newUser) => {
    const serverUserData = await fetchHelper.getUser(newUser);
    setUserData(serverUserData);
    AsyncStorage.setItem(USER_STORAGE_KEY, newUser);
    setUser(newUser);
  };
  /////////////////FIN GESTION USER/////////////////////////

  const [markers, setMarkers] = useState([]);

  //récupération des markers
  const getMarkersFromFirebase = async () => {
    let markersFirebase = await fetchHelper.getMarkers(user)
    setMarkers(markersFirebase)
  }

  useEffect(() => {
    //Annuler anciennes notifs
    Notifications.cancelAllScheduledNotificationsAsync();

    //Connexion
    initialProcess()

    //Localisation
    getLocationAsync()

    //Markers
    getMarkersFromFirebase()

    //Annuler nouvelles notifs
    registerBackgroundFetchAsync();
  }, []);



  //Affichage des markers
  function ListMarker() {
    if (markers != []) {
      return (
        markers.map((marker, key) => {
          if (marker.difficulty > 7) {
            return (
              < Marker
                key={key}
                title={marker.title}
                description={marker.hint}
                coordinate={{ "latitude": marker.latitude, "longitude": marker.longitude }}
                pinColor="#ff6961"
              />
            )
          }
          else if (marker.difficulty < 7 && marker.difficulty >= 4) {
            return (
              < Marker
                key={key}
                title={marker.title}
                description={marker.hint}
                coordinate={{ "latitude": marker.latitude, "longitude": marker.longitude }}
                pinColor="#ff9c42"
              />
            )
          }
          else {
            return (
              < Marker
                key={key}
                title={marker.title}
                description={marker.hint}
                coordinate={{ "latitude": marker.latitude, "longitude": marker.longitude }}
                pinColor="#93eeaa"
              />
            )
          }
        }

        )
      )
    }
  }

  //Template Home
  function HomeScreen({ navigation }) {
    useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
        getMarkersFromFirebase()
      });

      return unsubscribe;
    }, [navigation])


    if (!isLocationActivate) {
      return (
        <View style={styles.container}>
          <Text>{message}</Text>
          <Button onPress={getLocationAsync} title="I activated location service" />
        </View>
      )
    }
    else if (locationResult == null || mapRegion == null) {
      return (
        <View style={styles.container}>
          <Text>{message}</Text>
        </View>
      )
    }
    else {
      return (
        <View>
          <MapView
            style={styles.map}
            initialRegion={{
              "latitude": mapRegion.latitude,
              "latitudeDelta": mapRegion.latitudeDelta,
              "longitude": mapRegion.longitude,
              "longitudeDelta": mapRegion.longitudeDelta,
            }}
            showsUserLocation={true}
            followsUserLocation={true}
          >
            {ListMarker()}
          </MapView>
          <View
            style={{
              position: 'absolute',
              bottom: '10%',
              alignSelf: 'center'
            }}
          >
            <Button title="Create marker" onPress={() => navigation.navigate('Create Marker')} />
          </View>
        </View>
      );
    }
  }

  //Template Liste
  function ListScreen() {
    return (
      <View>
        <ListCache
          markers={markers}
        />
      </View>
    );
  }

  //Template profil
  function UserScreen({ navigation }) {
    return (
      <View>
        <User
          user={user}
          userData={userData}
          signOut={signOut}
          changeAvatarUrl={changeAvatarUrl}
          navigation={navigation}
        />
      </View>
    )

  }

  //Template register
  function RegisterScreen() {
    return (
      <View>
        <Register
        />
      </View>
    )
  }

  //Template register
  function ForgotPasswordScreen() {
    return (
      <View>
        <ForgotPassword
        />
      </View>
    )
  }


  //Template profil
  function EditUserScreen({ navigation }) {
    return (
      <View>
        <EditUser
          user={user}
          navigation={navigation}
        />
      </View>
    )
  }

  //Template marker
  function CreateMarkerScreen({ navigation }) {
    return (
      <View>
        <CreateMarker
          latitude={locationResult.latitude}
          longitude={locationResult.longitude}
          user={user}
          navigation={navigation}
        />
      </View>
    )
  }

  //Template corps déconnecté
  function AuthScreen({ navigation }) {
    return (
      <Auth setUser={setUserAndFetchData} navigation={navigation} />
    )
  }


  //Template stack page home/createMarker
  function stackNavHome() {
    return (
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Create Marker" component={CreateMarkerScreen} />
      </Stack.Navigator>
    )
  }

  //Template stack page user/editUser
  function stackNavUser() {
    return (
      <Stack.Navigator initialRouteName="User">
        <Stack.Screen name="User" component={UserScreen} />
        <Stack.Screen name="Edit User" component={EditUserScreen} />
      </Stack.Navigator>
    )
  }

  //Template stack page login/register
  const stackNavRegister = (
    <NavigationContainer>
      <StatusBar />
      <Stack.Navigator initialRouteName="Log in">
        <Stack.Screen name="Log in" component={AuthScreen} />
        <Stack.Screen name="Forgot Password" component={ForgotPasswordScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );

  //Template corps connecté
  const authenticatedBody = (
    <NavigationContainer>
      <StatusBar />
      <Tab.Navigator>
        <Tab.Screen
          name="HomeNav"
          component={stackNavHome}
          options={{
            headerShown: false,
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="List of caches"
          component={ListScreen}
          options={{
            tabBarLabel: 'List',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="format-list-bulleted" color={color} size={size} />
            ),
            tabBarBadge: markers.length,
          }}
        />
        <Tab.Screen
          name="Profile"
          component={stackNavUser}
          options={{
            headerShown: false,
            tabBarLabel: 'Profile',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="account" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );

  const body = user ? authenticatedBody : stackNavRegister;

  return userLoading ? <ActivityIndicator /> : body;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: "100%",
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

export default App;