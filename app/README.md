# Projet Appli web - GéoTrouveMoi

[insérer une description]

## Installation

1. Clonez ce projet dans un dossier

2. À l'intérieur de ce dossier, tapez ces commandes :

```shell
npm install
```
puis
```shell
npm start
```

3. Installez l'application Expo sur votre téléphone et scannez le QRcode après avoir choisi le type de Connection "Tunnel"

4. Patientez un peu, ça peu etre long...

5. Voila ! Vous pouvez voir votre appli web !
