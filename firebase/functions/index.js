const functions = require("firebase-functions");
const admin = require("firebase-admin");
const handleUserRequest = require("./user.js");
const handleCacheRequest = require("./cache.js");

admin.initializeApp();

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

// exports.helloWorld = functions
//     .region("europe-west1")
//     .https.onRequest((request, response) => {
//       functions.logger.info("Hello logs!", {structuredData: true});
//       response.send("Hello from Firebase!");
//     });

exports.user = functions
    .region("europe-west1")
    .https.onRequest(handleUserRequest);

exports.cache = functions
    .region("europe-west1")
    .https.onRequest(handleCacheRequest);
