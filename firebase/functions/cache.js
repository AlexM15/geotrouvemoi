const admin = require("firebase-admin");
const functions = require("firebase-functions");

const getAllCaches = (response) => {
  const cachesCollection = admin.firestore().collection("caches");

  cachesCollection
      .get()
      .then((data) => {
        if (data.docs.length == 0) {
          response.status(404).send({error: "There is no caches available"});
        } else {
          const caches = [];
          data.docs.forEach((item) => {
            caches.push(item.data());
          });

          functions.logger.debug(JSON.stringify(caches));
          response.send(caches);
        }
      });
};

const getCache = (cacheId, response) => {
  const cachesCollection = admin.firestore().collection("caches");

  cachesCollection
      .doc(cacheId)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          response.status(404).send({error: "Not found"});
        } else {
          response.send(doc.data());
        }
      });
};

const updateCache = (cacheId, request, response) => {
  const cachesCollection = admin.firestore().collection("caches");

  cachesCollection
      .doc(cacheId)
      .set(request.body, {merge: true})
      .then(() => {
        cachesCollection
            .doc(cacheId)
            .get()
            .then((doc) => {
              if (!doc.exists) {
                response.status(404).send({error: "Not found"});
              } else {
                response.send(doc.data());
              }
            });
      })
      .catch((err) => {
        functions.logger.error("Update Todo failed!", {error: err});
        response.status(500).send(err);
      });
};

const addCache =
  (request, response) => {
    functions.logger.info(request.body.title);
    if (request.body.title === "") {
      return response.status(400).send({title: "Title must not be empty"});
    }

    if (request.body.latitude === "") {
      return response.status(400).send(
          {latitude: "Latitude must not be empty"}
      );
    }

    if (request.body.longitude === "") {
      return response.status(400).send(
          {longitude: "Longitude must not be empty"}
      );
    }

    if (request.body.difficulty === "") {
      return response.status(400).send(
          {difficulty: "Difficulty must not be empty"}
      );
    }

    if (request.body.user === "") {
      return response.status(400).send(
          {user: "User must not be empty"}
      );
    }

    const newCacheItem = {
      title: request.body.title,
      latitude: request.body.latitude,
      longitude: request.body.longitude,
      hint: request.body.hint,
      difficulty: request.body.difficulty,
      createdAt: new Date().toISOString(),
      user: request.body.user,
    };

    const cachesCollection = admin.firestore().collection("caches");
    functions.logger.info("hello world ! 8");
    try {
      cachesCollection
          .add(newCacheItem)
          .then((doc) => {
            functions.logger.info("hello world ! 9");

            const responseCacheItem = newCacheItem;
            responseCacheItem.id = doc.id;
            response.send(responseCacheItem);
          })
          .catch((err) => {
            functions.logger.info("hello world ! 10");

            response.status(500).send({error: "Something went wrong"});
          });
    } catch (e) {
      functions.logger.error(e);
    }
  };

const handleCacheRequest = (request, response) => {
  const cacheId = request.query.cacheId;

  switch (request.method) {
    case "GET":
      if (!cacheId) {
        getAllCaches(response);
      } else {
        // Send back the cache
        getCache(cacheId, response);
      }
      break;
    case "PATCH":
      // Update the cache
      updateCache(cacheId, request, response);
      break;
    case "POST":
      // add the cache
      addCache(request, response);
      break;
    default:
      response.status(400).send({error: "Case Not allowed"});
      break;
  }
};

module.exports = handleCacheRequest;
