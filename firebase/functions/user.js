const admin = require("firebase-admin");
const functions = require("firebase-functions");

const getUser = (response, userId) => {
  const usersCollection = admin.firestore().collection("users");

  usersCollection
      .doc(userId)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          response.status(404).send("Not found");
        } else {
          response.send(doc.data());
        }
      });
};

const updateUser = (response, userId, body) => {
  const usersCollection = admin.firestore().collection("users");

  usersCollection
      .doc(userId)
      .set(body, {merge: true})
      .then(() => {
        usersCollection
            .doc(userId)
            .get()
            .then((doc) => {
              if (!doc.exists) {
                response.status(404).send("Not found");
              } else {
                response.send(doc.data());
              }
            });
      })
      .catch((err) => {
        functions.logger.error("Update Todo failed!", {error: err});
        response.status(500).send(err);
      });
};

const createUser =
  (request, response) => {
    if (request.body.name === "") {
      return response.status(400).send({name: "Name must not be empty"});
    }

    const newUserItem = {
      name: request.body.name,
      avatar: "https://firebasestorage.googleapis.com/v0/b/geotrouvemoi-330406.appspot.com/o/no-user-image-icon.jpg?alt=media&token=344091cd-cc2a-45c5-85d3-329f4bffaf7b",
      createdAt: new Date().toISOString(),
    };

    const usersCollection = admin.firestore().collection("users");
    functions.logger.info("hello world ! 8");
    try {
      usersCollection
          .doc(request.body.email)
          .set(newUserItem)
          .then((doc) => {
            functions.logger.info("hello world ! 9");

            const responseUserItem = newUserItem;
            responseUserItem.email = doc.id;
            response.send(responseUserItem);
          })
          .catch((err) => {
            functions.logger.info("hello world ! 10");

            response.status(500).send({error: "Something went wrong"});
          });
    } catch (e) {
      functions.logger.error(e);
    }
  };

const handleUserRequest = (request, response) => {
  const userId = request.query.userId;
  const body = request.body;
  switch (request.method) {
    case "GET":
      if (!userId) response.status(400).send("Not allowed");
      else {
        // Send back the user
        getUser(response, userId);
      }
      break;
    case "PATCH":
      if (!userId) response.status(400).send("Not allowed");
      else {
        // Update the user
        updateUser(response, userId, body);
      }
      break;
    case "POST":
      // Create the user
      createUser(request, response);
      break;
    default:
      response.status(400).send("Not allowed");
      break;
  }
};

module.exports = handleUserRequest;
